package com.classpath.ordermicroservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name="line_items")
public class LineItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;
    private double price;
    private int qty;
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;
}
